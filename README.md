# Mean builder
# Table of contents

1. Installation
2. Simple Use
    1. First Time
    1. After that
    1. Production
3. Options
    1. Root
    1. Output Path
    1. Frontend Path
    1. Backend Path
    1. Ignore Files
    1. Ignore Folders
    1. Angular Output Path
    1. Angular Base href
4. Flags
    1. Use git
    1. Use TypeScript
    1. Production
    1. Dump

# Installation

```bash
npm install -g mean-builder
```

# Simple Use

To use this you must have a folder structure like

* root
  * backend - (express app)
  * frontend - (angular app) 

### 1. First time

First time building on a localmachine

```bash
mean-builder --use-ts
```

### 2. After that

Use thing when you want to build and have a git repo in the folder

```bash
mean-builder --use-git --use-ts
```

### 3. Production build

```bash
mean-builder --use-git --use-ts --prod
```

----

# Options

```bash
mean-builder --(option)="(value)"
```

----

## Root

__Command:__  _--root="(path_from_Local)"_

__Default:__ _Where the program opened from_

__Example:__

Go back a folder

```bash
mean-builder --root="../"
```

----

## Output Path

__Command:__  _--output-path="(path_to_output)"_

__Default:__ _/dist_

__Example:__

Change from /dist to /build

```bash
mean-builder --output-path="/build"
```

----

## Frontend Path

__Command:__  _--frontend-path="(path_to_frontend)"_

__Default:__ _/frontend_

__Example:__

Change from /frontend to /myangularapp

```bash
mean-builder --frontend-path="/myangularapp"
```

----

## Backend Path

__Command:__  _--backend-path="(path_to_backend)"_

__Default:__ _/backend_

__Example:__

Change from /backend to /myexpressapp

```bash
mean-builder --backend-path="/myexpressapp"
```

----

## Ignore Files

In root or subfolder dosen't matter

__Command:__  _--ignore-files="(filenames, comma seperated)"_

__Default:__ _NONE_

__Example:__

Ignore file bootstrap.min.js and jquery.min.js

```bash
mean-builder --ignore-files="bootstrap.min.js,jquery.min.js"
```

----

## Ignore Folders

This will ignore alld files and subfolders aswell

__Command:__  _--ignore-folders="(foldernames, comma seperated)"_

__Default:__ _NONE_

__Example:__

Ignore the assets folder

```bash
mean-builder --ignore-folders="assets"
```

----


## Angular Output Path

__Command:__  _--ng-output-path="(save_folder_inside_output_path>)"_

__Default:__ _public_

__Example:__

Change from /public to root

```bash
mean-builder --ng-output-path="../"
```

----

## Angular Base href

Angular-cli --base-href

__Command:__  _--ng-base-href="(subfolder_on_webserver)"_

__Default:__ _/_

__Example:__

Change from root to /sideproject

```bash
mean-builder --ng-base-href="/sideproject"
```

----

# Flags

```bash
mean-builder --(flag)
```

----

## Use git

__Command:__  _--use-git_

__Function:__ _Stops removal of .git, .gitignore, *.md in dist folder_

__Use:__

```bash
mean-builder --use-git
```

----

## Use TypeScript

__Command:__  _--use-ts_

__Function:__ _Compiles typescript backend_

__Use:__

```bash
mean-builder --use-ts
```

----

## Production

__Command:__  _--prod_

__Function:__ _Compiles typescript and builds angular in production mode_

__Use:__

```bash
mean-builder --prod
```

----

## Dump

__Command:__  _--dump_

__Function:__ _dumps all options and flags to the console. (Aborts the building)_

__Use:__

```bash
mean-builder --dump
```