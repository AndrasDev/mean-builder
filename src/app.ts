#!/usr/bin/env node

import Command from './controllers/command';
import * as fs from 'fs';
import * as path from 'path';
import {exec, execSync} from 'child_process';

let cmd = new Command('mean-builder');

cmd.addParamater({identifier: "--root", name: "root", defaultValue: process.cwd()});

cmd.addParamater({identifier: "--output-path", name: "outputPath", defaultValue: '/dist'});
cmd.addParamater({identifier: "--backend-path", name: "backendPath", defaultValue: '/backend'});
cmd.addParamater({identifier: "--frontend-path", name: "frontendPath", defaultValue: '/frontend'});

cmd.addParamater({identifier: "--ng-base-href", name: "ngBasehref", defaultValue: '\\'});
cmd.addParamater({identifier: "--ng-output-path", name: "ngOutputPath", defaultValue: 'public'});
cmd.addParamater({identifier: "--ignore-files", name: "ignoreFiles"});
cmd.addParamater({identifier: "--ignore-folders", name: "ignoreFolders"});

cmd.addFlag({identifier: "--use-git", name: "useGit"});
cmd.addFlag({identifier: "--use-ts", name: "useTs"});
cmd.addFlag({identifier: "--prod", name: "prod"});

cmd.addFlag({identifier: "--dump", name: "dump"});


try
{
    cmd.compute(process.argv);
} catch (error) 
{
    console.log(error);
    process.exit(-1);
}

let outputFolderPath = path.join(cmd.getParamater('root'), cmd.getParamater('outputPath'));
let frontendFolderPath = path.join(cmd.getParamater('root'), cmd.getParamater('frontendPath'));
let backendFolderPath = path.join(cmd.getParamater('root'), cmd.getParamater('backendPath'));

if (cmd.getFlag('dump')) {
    console.log();
    console.log('args: ' + process.argv)
    console.log();
    console.log('root: ' + path.join(cmd.getParamater('root')));
    console.log();
    console.log('outputPath: ' + outputFolderPath);
    console.log('backendPath: ' + backendFolderPath);
    console.log('frontendPath: ' +frontendFolderPath);
    console.log();
    console.log('ngBasehref: ' + cmd.getParamater('ngBasehref'));
    console.log('ngOutputPath: ' + path.join(outputFolderPath, cmd.getParamater('ngOutputPath')));
    console.log();
    console.log('ignoreFiles: ' + cmd.getParamater('ignoreFiles'));
    console.log('ignoreFolders: ' + cmd.getParamater('ignoreFolders'));
    console.log();
    console.log('useGit: ' + cmd.getFlag('useGit'));
    console.log('useTs: ' + cmd.getFlag('useTs'));
    console.log('prod: ' + cmd.getFlag('prod'));
    console.log('dump: ' + cmd.getFlag('dump'));

    process.exit();
}



if(!fs.existsSync(outputFolderPath))
{
    fs.mkdirSync(outputFolderPath)
}
else
{
 
    const isDirectory = (source: any) => fs.lstatSync(source).isDirectory();
    const isFile = (source: any) => fs.lstatSync(source).isFile();

    const getDirectories = (source: any) => fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory);
    const getFiles = (source: any) => fs.readdirSync(source).map(name => path.join(source, name)).filter(isFile);

    let ignoreFiles = Array<string>();

    let paramIgnoreFiles = cmd.getParamater('ignoreFiles');

    if (paramIgnoreFiles != false) {
        ignoreFiles = paramIgnoreFiles.split(',');
    }

    if (cmd.getFlag('useGit')) {
        ignoreFiles.push('*.md');
        ignoreFiles.push('.gitignore');
    }

    for (let i = 0; i < ignoreFiles.length; i++) {
        ignoreFiles[i] = ignoreFiles[i].replace('*', '');
    }

    let ignoreFolders = Array<string>();

    let paramIgnoreFolders = cmd.getParamater('ignoreFolders');

    if (paramIgnoreFolders != false)
    {
        ignoreFolders = paramIgnoreFolders.split(',');
    }

    if (cmd.getFlag('useGit')) {
        ignoreFolders.push('.git');
    }


    ignoreFolders.push('node_modules');

    for (let i = 0; i < ignoreFolders.length; i++) {
        ignoreFolders[i] = ignoreFolders[i].replace('*', '');
    }

    const removeFilesAndDirectorys = (path: any, removefolder: boolean = true) =>
    {

        let filesToRemove = new Array<any>();
        let DirectoriesToRemove = new Array<any>();

        getFiles(path).forEach((file: string) =>
        {
            let removeFile = true;

            ignoreFiles.forEach((pattern: string) => {
                if (file.endsWith(pattern))
                    removeFile = false;
            });

            if (removeFile) {
                filesToRemove.push(file);
            }
        });

        getDirectories(path).forEach((directory: string) =>
        {
            let removeDirectory = removefolder;

            ignoreFolders.forEach((pattern: string) => {
                if (directory.endsWith(pattern))
                    removeDirectory = false;
            });

            if (removeDirectory)
            {
                removeFilesAndDirectorys(directory, removeDirectory);
                DirectoriesToRemove.push(directory);                
            }
            
        });

        filesToRemove.forEach(element => {
            fs.unlinkSync(element);
        });

        DirectoriesToRemove.forEach(element => {
            fs.rmdirSync(element);
        });
    } 

    removeFilesAndDirectorys(outputFolderPath);
}

let buildCommand;

if (cmd.getFlag('useTs'))
{
    console.log('-------    REWRITING CONFIG        -------');

    let tsConfigPath = path.join(backendFolderPath, 'tsconfig.json');

    let obj: any = JSON.parse(fs.readFileSync(tsConfigPath).toString());
    let devDir = obj.compilerOptions.outDir;
    obj.compilerOptions.outDir = outputFolderPath;
    fs.writeFileSync(tsConfigPath, JSON.stringify(obj));
 
    buildCommand = "tsc";

    if (cmd.getFlag('prod')) {
        buildCommand += " --build";
    }

    console.log('-------    COMPILING TYPESCRIPT    -------');
    execSync(buildCommand, {cwd:  backendFolderPath});
    console.log('-------        COMPILE DONE        -------');

    obj.compilerOptions.outDir = devDir;
    fs.writeFileSync(tsConfigPath, JSON.stringify(obj));
}
else
{
    console.log('NOT YET IMPLEMENTED');
    process.exit(-1);
}



console.log('-------    COPYING NPM           -------');
fs.writeFileSync(path.join(outputFolderPath, 'package.json'), fs.readFileSync(path.join(backendFolderPath, 'package.json')));
console.log('-------    COPYING DONE          -------');

console.log('-------    INSTALLING PACKAGE    -------');
execSync("npm i", {cwd:  outputFolderPath});
console.log('-------        INSTALATION DONE        -------');

buildCommand = "ng build --output-path " + path.join(outputFolderPath, cmd.getParamater('ngOutputPath'));

if (cmd.getFlag('prod'))
{
    buildCommand += " --prod";
}

if (cmd.getParamater('ngBasehref') != false)
{
    buildCommand += " --base-href " + cmd.getParamater('ngBasehref');
}


console.log('-------    BUILDING ANGULAR    -------');
console.log('Angular Build Command: ' + buildCommand);

execSync(buildCommand, {cwd:  frontendFolderPath});
console.log('-------        BUILD COMPLETE        -------');

