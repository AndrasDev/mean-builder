import * as path from "path";

export default class Command
{
    private readonly programName: string;
    private currentFolder: string;

    private paramaters: Array<any>;
    private paramatersNameIndex: any;

    constructor(programName: string)
    {
        this.programName = programName;
        this.paramaters = new Array<any>();
        this.paramatersNameIndex = {};
    }

    

    addParamater(param: {identifier: string, name: string, isFlag?: boolean, defaultValue?: any})
    {
        if (this.paramatersNameIndex[param.name] !== undefined)
            throw new Error("Paramater with name " + param.name + " already exists");
            
        if (param.isFlag === undefined)
            param.isFlag = false;

        if (param.defaultValue === undefined)
            param.defaultValue = false;

        this.paramatersNameIndex[param.name] = this.paramaters.length;
        this.paramaters.push({ isFlag: param.isFlag, value: param.defaultValue, pattern: param.identifier});
    }

    addFlag(param: {identifier: string, name: string, defaultValue?: any})
    {
        this.addParamater({identifier: param.identifier, name: param.name, defaultValue: param.defaultValue, isFlag: true});
    }

    getParamater(name: string): any
    {
        if (this.paramatersNameIndex[name] === undefined)
            return false;
        
        let param = this.paramaters[this.paramatersNameIndex[name]];
        
        return param.value
    }

    getFlag(name: string): any
    {
        return this.getParamater(name);
    }

    compute(args: string[])
    {
        if (args.length == 0)
            return;

        if (args[0] !== this.programName)
            this.currentFolder = null;

        for (let i = 2; i < args.length; i++)
        {

            let validCommand = false;

            if (args[i][0] === '-')
            {
                let arg = args[i];

                for (let i = 0; i < this.paramaters.length; i++)
                {
                    let item = this.paramaters[i];

                    if (item.isFlag)
                    {
                        if (arg.startsWith(item.pattern))
                        {
                            if (arg !== item.pattern)
                                throw new Error("You can't use a flag as a paramater");

                            this.paramaters[i].value = true; 
                            validCommand = true;
                        }
                    }
                    else
                    {
                        let split = arg.split('=');

                        if (split[0] === item.pattern as string)
                        {

                            if (split[1] === undefined)
                                throw new Error("You can't use a paramater as a flag");
                            
                            
                            if (split[0] == "--root")
                            {
                                this.paramaters[i].value = path.join(this.paramaters[i].value, split[1])
                                validCommand = true;
                            }
                            else
                            {
                                this.paramaters[i].value = split[1];
                                validCommand = true;
                            }
                        }
                    }
                }
            }

            if (!validCommand)
            {
                throw new Error(args[i] + " is an invalid paramater/flag");       
            }
        }
    }
}